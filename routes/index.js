var _ = require('underscore');
var qs = require('qs');

/* GET home page. */


module.exports = function(app, io) {

    app.get('/', function(req, res) {
        res.render('index');
    });

    var users = {};
    var current_price = 100;


    io.sockets.on('connection', function(client) {
        var username="";

        client.on('disconnect', function () {
            client.broadcast.emit('gamer:remove', {name: username });
            delete users[username]
        });


        client.on('bet:create', function(data) {

            var accepted_bet = get_bets_from_client(users,username, data);
            var sum_scores= get_sum_of_scores(users);
            var leaders =get_leaders(users,sum_scores);
            var price = 0;
            if ((current_price - sum_scores) > 0)
                price = current_price - sum_scores;
            client.broadcast.emit('reduce price', price );
            client.emit('reduce price', price );

            client.broadcast.emit('leaders:list', leaders );
            client.emit('leaders:list', leaders );
            client.emit('bet:list', accepted_bet );



            if (price <= 99)
                stopwatch.start();


        });

        client.on('join', function(name){
            username = name;
//            client.broadcast.emit('gamer:add', name);
//            client.emit('gamer:add', name);
//            Object.keys(users).forEach(function(name){
//                client.emit('gamer:add', name);
//            });
            users[name] = [[0,0], [0, 0], [0,0]];
            var sum_scores= get_sum_of_scores(users);
            var leaders =get_leaders(users,sum_scores);
            client.emit('leaders:list', leaders );
            var price = 0;
            if ((current_price - sum_scores) > 0)
                price = current_price - sum_scores;
            client.emit('reduce price', price );



//        redisClient.sadd('gamers', name);
//        redisClient.smembers('gamers', function(err, names) {
//            client.broadcast.emit('reduce price', names.length);
//            client.emit('reduce price', names.length);
//
//            names.forEach(function(name){
//                client.emit('add gamer', name);
//            });
//        });
        });
    });


    function get_bets_from_client(users, name, bets){
        var accepted_bet =[]
        if (users && name && bets) {
            for (i = 0; i < 3; i++) {
                users[name][i][0] = parseInt(bets[i][0]);
                users[name][i][1] = parseInt(bets[i][1]);
                if ((!isNaN(users[name][i][1])) && (!isNaN(users[name][i][0])))
                    accepted_bet.push(users[name][i])
            }
        }
        return accepted_bet
    }
    function get_sum_of_scores(users){
        var sum_scores =0;
        if (users) {
            for (u in users) {
                for (i = 0; i < 3; i++) {
                    if (!isNaN(users[u][i][1])) {
                        sum_scores += users[u][i][1];
                    }
                }
            }
        }
        return sum_scores;
    }

    function get_leaders(users, sum_scores){
        var leaders ={};
        if (users && leaders) {
            for (u in users) {
                for (i = 0; i < 3; i++) {
                    if ((!isNaN(users[u][i][1])) && ( users[u][i][0] >= (current_price - sum_scores))) {
                        var b = users[u][i][1] * ((current_price - users[u][i][0]) / (sum_scores) )
                        if ((b > leaders[u]) || !(u in leaders))
                            leaders[u] = b;
                    }
                }
            }
        }
        var la = _.map(leaders, function(k,v){ return [Math.round(k*100)/100,v]; });
        return _.sortBy(la, function(el){ return el[0] }).reverse().slice(0, 3);
    }


    var Stopwatch = require('../models/stopwatch');
    var stopwatch = new Stopwatch();

    stopwatch.on('tick:stopwatch', function(time) {
        io.sockets.emit('time:tick', time);
    });
    stopwatch.on('stop:stopwatch', function() {
        var sum_scores= get_sum_of_scores(users);
        var leaders =get_leaders(users,sum_scores);
        io.sockets.emit('winner', [leaders[0], current_price - sum_scores ]  );
        io.sockets.emit('time:stop', '');
        stopwatch = new Stopwatch();
    });


}