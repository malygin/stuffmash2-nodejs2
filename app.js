var express = require('express');
var path = require('path');
//var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app 	= express();

var port = process.env.PORT || 5000
var server 	= require('http').createServer(app);
var io 		= require('socket.io').listen(server);
var routes = require('./routes/index');
//var users = require('./routes/users');


app.get('/partials/:name', function (req, res) {
    var name = req.params.name;
    res.render('partials/' + name);
});
app.get('/templates/:name', function (req, res) {
    var name = req.params.name;
    res.render('templates/' + name);
});

app.set('views', __dirname+'/views/' );
app.set('view engine', "jade");
app.engine('jade', require('jade').__express);
app.use(express.static(__dirname + '/public'));

//app.get("/", function(req, res){
//    res.render("index");
//});

routes(app, io);



server.listen(port);
