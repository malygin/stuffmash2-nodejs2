var server = io.connect('');
var name ="";
var width = 800;
var height = 500;


//server.on('add gamer', insertChatter);

//server.on('time', function(time) {
//    $('#countdown').html(time);
//});
//
//server.on('winner', function(data) {
//   alert("Скидку выиграл " + data+"!  Поздравляем!!");
//});

//server.on('reduce price', function(price) {
//    data[0].data.push({ x: i, y: price });
//    i = i+1;
////    graph.render();
//   $('#current-price').html(price);
//});

//server.on('accept bet', function(data){
//    var s ='';
//    data.forEach(function(el){
//        s += '<h5> на '+ el[0] +' руб. <small> очков: '+  el[1]+'</small></h5>';
//    });
//    $('#bets').html(s);
//});
//
//server.on('leaders', function(data){
//    var s ='';
//
//    data.forEach(function(k){
//    s += '<li> '+ k[1] +'</li>';
//    });
//    $('#leaders').html(s)
//});

//$('#submitBet').click(function(e){
//    server.emit('make bet',[name, $('#formBets').serialize()]);
//
//    e.preventDefault();
//});
//$('.bets').keyup(function(){
//    if ($(this).val() > 100){
//        alert("Цена не более 100!!");
//        $(this).val('100');
//    }
//});
//$('.scores').keyup(function(){
//    var sum =0;
//    $('.scores').each(function( el ) {
//        sum =sum + parseInt($(this).val());
//        if (sum > 15){
//            alert("У вас только 15 очков!!");
//            $(this).val(0);
//            return;
//        }else{
//            $('#submitBet').click();
//
//        };
//    });
//
//});
// instantiate our graph!

var data = [
    {
        data: [ { x: 0, y: 100 } ],
        color: "#cde9ff"
    }
];
var i = 0;

var graph = new Rickshaw.Graph( {
    element: document.getElementById("chart"),
    renderer: 'area',
    height: 250,
    width: $('.panel-success-lt').width() - 30 ,
    stroke: true,
    preserve: true,
    series: data
} );

var y_ticks = new Rickshaw.Graph.Axis.Y( {
    graph: graph,
    orientation: 'left',
    height: 250,
    tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
    tickValues: [0,10,20,30,40,50,60, 70, 80, 90, 100],
    element: document.getElementById('y_axis')
} );

//
graph.render();

var drag = d3.behavior.drag()
    .on("dragend", function(d,i){

    initialY1 = this.getAttribute("y"),
    initialY2 = this.getAttribute("y2");

    $('.' +this.getAttribute("class_slider")).val(parseInt(100 - (+initialY1) * 100 /250)).trigger('change');
    angular.element(document.getElementById('bets')).scope().sendBets();

//    $('#submitBet').click();

    })
    .on("drag", function(d,i){
        var
            initialY1 = this.getAttribute("y"),
            initialY2 = this.getAttribute("y2");

       $('.' +this.getAttribute("class_slider")).val(parseInt(100 - (+initialY1 + d3.event.dy) * 100 /250));
        d3.select(this)
            .attr("x", 0)
            .attr("y", +initialY1 + d3.event.dy)
//            .attr("width", 100)
//            .attr("height", 3);
//        $('#submitBet').click();
    });

function draw_lines(bets){
    var svg  = d3.select('#chart svg');
    svg.selectAll("rect").remove()
    if (!isNaN(parseInt(bets[0][0]))){
        var y_1 = (100 - bets[0][0]) * (250/100);
        svg.append("svg:rect").attr("x", 0).attr("y",y_1) .attr("width", $('.panel-success-lt').width() - 5).attr("height",1).attr("class_slider",'knob1').call(drag).style("fill", "#e33244").style("stroke", "black").style("stroke-width", "15px").style("stroke-opacity", "0.01");}

    if (!isNaN(parseInt(bets[1][0]))){
        var y_1 = (100 - bets[1][0]) * (250/100);
        svg.append("svg:rect").attr("x", 0).attr("y",y_1) .attr("width", $('.panel-success-lt').width() - 5).attr("height",1).attr("class_slider",'knob2').style("fill", "#fbb23e").style("stroke", "black").style("stroke-width", "15px").style("stroke-opacity", "0.01").call(drag);}
    if (!isNaN(parseInt(bets[2][0]))){
        var y_1 = (100 - bets[2][0]) * (250/100);
        svg.append("svg:rect").attr("x", 0).attr("y",y_1) .attr("width", $('.panel-success-lt').width() - 5).attr("height",1).attr("class_slider",'knob3').style("fill", "#1aae88").style("stroke", "black").style("stroke-width", "15px").style("stroke-opacity", "0.01").call(drag);}
}

window.onload = function(){

    name = prompt("Ваше имя","Иван Иванов");
    //insertChatter(name);
    server.emit('join', name);

}