'use strict';

/* Controllers */

angular.module('smApp.controllers', []).
    controller('ListLeadersCtrl', function ($scope, socket) {
        $scope.leaders =[];
        socket.on('leaders:list', function (data) {
            $scope.leaders = data;
        });
    }).
    controller('timerCtrl', function ($scope, socket) {
        $scope.timer ="";
        socket.on('time:tick', function (data) {
            $scope.timer = data;
        });
        socket.on('time:stop', function (data) {
            $scope.timer = "";
        });
        socket.on('winner', function (data) {
            alert("Пачку печенья по цене " + data[1]+" забирает " +data[0][1] + ", коэффициент выигрышной ставки - "+ data[0][0]);
        });
    }).
    controller('ListUsersCtrl', function ($scope, socket) {
        $scope.users =[];
        socket.on('gamer:add', function (data) {
            $scope.users.push(data);
        });
        socket.on('gamer:remove', function (data) {
            $scope.users.splice($scope.users.indexOf('data'),1);
        });
        socket.on('disconnect', function (data) {
            $scope.users = [];
            alert('На сервере запущен новый раунд игры, обновите, пожалуйста, страничку, чтобы войти в игру!');
        });
    }).
    controller('BetsController', function($scope, socket){
        $scope.bet1=[0,0];
        $scope.bet2=[0,0];
        $scope.bet3=[0,0];
        $scope.bets=[];
        $scope.max_score = _.random(5, 15);


        $scope.sendBets = function () {
            server.emit('bet:create',[$scope.bet1, $scope.bet2, $scope.bet3]);
        };

        $scope.updateScore = function(score) {
            if ((parseInt($scope.bet1[1]) + parseInt($scope.bet2[1]) +parseInt($scope.bet3[1])) > $scope.max_score){
                alert('у вас только 10 очков');
                score[1]=0;
            }

        };
        $scope.updatePrice = function(price) {
            if (parseInt(price[0]) > 100){
                alert('цена не более 100');
                price[0]=0;
            }

        }
        socket.on('bet:list', function (data) {
            $scope.bets = data;
            if ($scope.bets.length >0)
                draw_lines($scope.bets);

        });
        socket.on('reduce price', function(price) {
            data[0].data.push({ x: i, y: price });
            i = i+1;
            graph.render();
            $('#chart svg').append($('#chart svg rect').detach());

            $('#current-price').html(price);
        });

    }).directive("counter", function() {
        return {
            restrict: "A",
            scope: {
                value: "=value",
                maxScore:"=score"
            },
            templateUrl:'/templates/counter',

            link: function(scope, element, attributes) {
                var max, min, setValue, step;
                max = void 0;
                min = void 0;
                setValue = void 0;
                step = void 0;
                if (angular.isUndefined(scope.value)) {
                    throw "Missing the value attribute on the counter directive.";
                }
                min = (angular.isUndefined(attributes.min) ? null : parseInt(attributes.min));
                max = (angular.isUndefined(attributes.max) ? null : parseInt(attributes.max));
                step = (angular.isUndefined(attributes.step) ? 1 : parseInt(attributes.step));
                element.addClass("counter-container");
                scope.readonly = (angular.isUndefined(attributes.editable) ? true : false);

                /**
                 Sets the value as an integer.
                 */
                setValue = function (val) {
                    scope.value = parseInt(val);

                };
                setValue(scope.value);

                /**
                 Decrement the value and make sure we stay within the limits, if defined.
                 */
                scope.minus = function () {
                    if (min && (scope.value <= min || scope.value - step <= min) || min === 0 && scope.value < 1) {
                        setValue(min);
                        return false;
                    }
                    setValue(scope.value - step);
                    scope.maxScore = scope.maxScore  + step;

                };

                /**
                 Increment the value and make sure we stay within the limits, if defined.
                 */
                scope.plus = function () {
                    if (max && (scope.value >= max || scope.value + step >= max )) {
                        setValue(max);
                        return false;
                    }
                    if (scope.maxScore == 0)
                        return false;
                    if (scope.value == 0)
                        console.log('1');
                    setValue(scope.value + step);

                    scope.maxScore = scope.maxScore  -step;
                };

                /**
                 This is only triggered when the field is manually edited by the user.
                 Where we can perform some validation and make sure that they enter the
                 correct values from within the restrictions.
                 */
                scope.changed = function () {
                    if (!scope.value) {
                        setValue(0);
                    }
                    if (/[0-9]/.test(scope.value)) {
                        setValue(scope.value);
                    } else {
                        setValue(scope.min);
                    }
                    if (min && (scope.value <= min || scope.value - step <= min)) {
                        setValue(min);
                        return false;
                    }
                    if (max && (scope.value >= max || scope.value + step >= max)) {
                        setValue(max);
                        return false;
                    }
                    setValue(scope.value);
                };

            }
        };
    })

    .directive("slider", function() {
        return {
            restrict: 'A',
            scope: {
                config: "=config",
                price: "=model"
            },
            link: function(scope, elem, attrs) {
                var setValue = function(value) {
                    scope.price = value;
                }

                $(elem).knob({
                    width: 100,
                    height: 100,
                    thickness: .2,
                    fgColor: $(elem).data('color'),
                    min: 0,
                    max: 100,
                    change: function( ui) {
//                        scope.$apply(function() {
//                            scope.price = ui;
//                        });
//                        console.log($(this).getAttribute('class'));
                        $('#submitBet').click();
                        console.log('1');

                    },
                    release: function (ui) {
                        scope.$apply(function() {
                            scope.price = ui;
                        });
                        $('#submitBet').click();
//                        console.log('!');
                    }
                });

//                scope.changed = function () {
//
//                    setValue(scope.price);
//                    $(elem).val( newValue ).trigger('change');
//
//                };
//                scope.changed('price', function(newValue, oldValue) {
//                    $(elem).val( newValue ).trigger('change');
//                    scope.$apply(function() {
//                        scope.price = newValue;
//                    });
//                    $('#submitBet').click();
//                });

            }
        }
    });