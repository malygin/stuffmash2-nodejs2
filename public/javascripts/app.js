'use strict';

// Declare app level module which depends on filters, and services
$(document).ready(function(){
     $('.em-knob').each(function () {
                var $el, color, max, min, val;
                $el = $(this);
                color = $el.data('color');
                min = 0;
                max = 100;
                val = 0;
                $el.val(val);
                return $el.knob({
                    width: 100,
                    height: 100,
                    thickness: .2,
                    fgColor: color,
                    min: min,
                    max: max
                });
            });

});

angular.module('smApp', [
    'ngRoute',

    'smApp.controllers',
    'smApp.filters',
    'smApp.services',
    'smApp.directives',

    // 3rd party dependencies
    'btford.socket-io'
]).
    config(function ($routeProvider, $locationProvider) {
        $routeProvider.
            when('/view1', {
                templateUrl: 'partials/partial1',
                controller: 'MyCtrl1'
            }).
            when('/view2', {
                templateUrl: 'partials/partial2',
                controller: 'MyCtrl2'
            });

        $locationProvider.html5Mode(true);
    });