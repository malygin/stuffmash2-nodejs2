debug = require("debug")("stuffmash-2-dev")
path = require("path")
favicon = require("serve-favicon")
logger = require("morgan")
cookieParser = require("cookie-parser")
bodyParser = require("body-parser")
express = require("express")
app = express()
server = require("http").createServer(app)
io = require("socket.io").listen(server)
redis = require("redis")
url = require("url")
qs = require("qs")
_ = require("underscore")
port = process.env.PORT or 5000

#if (process.env.REDISCLOUD_URL) {
#    var redisURL = url.parse(process.env.REDISCLOUD_URL);
#    var redisClient = redis.createClient(redisURL.port, redisURL.hostname, {no_ready_check: true});
#    redisClient.auth(redisURL.auth.split(":")[1]);
#} else {
#   var redisClient = redis.createClient();
#}

#io.set(socketSession.parser);
app.set "views", path.join(__dirname, "views")
app.set "view engine", "jade"
app.use logger("dev")
app.use bodyParser.json()
app.use bodyParser.urlencoded(extended: false)
app.use cookieParser()
app.use express.static(path.join(__dirname, "public"))
app.get "/", (req, res) ->
  res.render "index"


users = {}
current_price = 100

io.sockets.on "connection", (client) ->
  client.on "make bet", (data) ->
    name = data[0]
    bets = qs.parse(data[1])
    accepted_bet = []
    leaders = {}
    i = 0
    while i < 3
      users[name][i][0] = parseInt(bets.bet[i].price)
      users[name][i][1] = parseInt(bets.bet[i].score)
      accepted_bet.push users[name][i]  if (not isNaN(users[name][i][1])) and (not isNaN(users[name][i][0]))
      i++

    console.log "hello"
    sum_scores = 0
    for u of users
      i = 0
      while i < 3
        sum_scores += users[u][i][1]  unless isNaN(users[u][i][1])
        i++
    for u of users
      i = 0
      while i < 3
        if (not isNaN(users[u][i][1])) and (users[u][i][0] >= (current_price - sum_scores))
          b = users[u][i][1] * ((current_price - users[u][i][0]) / (sum_scores))
          leaders[u] = b  if (b > leaders[u]) or (u of leaders)
        i++
    client.broadcast.emit "reduce price", current_price - sum_scores
    client.emit "reduce price", current_price - sum_scores
    client.broadcast.emit "leaders", leaders
    client.emit "leaders", leaders
    client.emit "accept bet", accepted_bet

  client.on "join", (name) ->
    console.log "join new gamer! " + name
    client.broadcast.emit "add gamer", name
    Object.keys(users).forEach (name) ->
      client.emit "add gamer", name

    users[name] = [[0,0], [0, 0], [0,0]];


server.listen(port);