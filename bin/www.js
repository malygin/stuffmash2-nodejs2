
var express = require('express');
var app 	= express();
var server 	= require('http').createServer(app);
var io 		= require('socket.io').listen(server);
var redis   = require('redis');
var url = require('url');
var qs = require('qs');


var port = process.env.PORT || 5000

var users = {};

var current_price = 100;

//var sessionSettings = {
//    "store":  session.MemoryStore,
//    "secret": "your secret",
//    "cookie": { "path": '/', "httpOnly": true, "secure": false,  "maxAge": null }
//};
//var socketSession = socketIOSession(sessionSettings);

//if (process.env.REDISCLOUD_URL) {
//    var redisURL = url.parse(process.env.REDISCLOUD_URL);
//    var redisClient = redis.createClient(redisURL.port, redisURL.hostname, {no_ready_check: true});
//    redisClient.auth(redisURL.auth.split(":")[1]);
//} else {
//   var redisClient = redis.createClient();
//}





app.set('views', __dirname );
app.set('view engine', "ejs");
app.engine('jade', require('jade').__express);
app.use(express.static( 'public'));

//global.io.configure(function () {
//    global.io.set("transports", ["xhr-polling"]);
//    global.io.set("polling duration", 10);
//});
var routes = require('../routes/index');
app.use('/', routes);

io.sockets.on('connection', function(client) {


    client.on('make bet', function(data) {
        var name = data[0];
        var bets = qs.parse(data[1]);
        var accepted_bet = [];
        var leaders = {};
        for (i = 0; i < 3; i++) {
            users[name][i][0] = parseInt(bets.bet[i].price);
            users[name][i][1] = parseInt(bets.bet[i].score);
            if ((!isNaN(users[name][i][1])) &&  (!isNaN(users[name][i][0])))
              accepted_bet.push(users[name][i])
        }
        console.log('hello');
        var sum_scores= 0;
        for (u in users){
            for (i = 0; i < 3; i++) {
                if (!isNaN(users[u][i][1])){
                    sum_scores+=users[u][i][1];
                }
            }
        }

        for (u in users){
            for (i = 0; i <3; i++){
                if ((!isNaN(users[u][i][1])) && ( users[u][i][0] >= (current_price - sum_scores)) ){
                    var b = users[u][i][1]  * ((current_price - users[u][i][0]) / (sum_scores) )
                     if ((b > leaders[u]) || !(u in leaders))
                        leaders[u] = b;
                }
            }
        }
        client.broadcast.emit('reduce price', current_price - sum_scores );
        client.emit('reduce price', current_price - sum_scores );

        client.broadcast.emit('leaders', leaders );
        client.emit('leaders', leaders );


        client.emit('accept bet', accepted_bet );


    });

    client.on('join', function(name){
        console.log('join new gamer! '+name);

        client.broadcast.emit('add gamer', name);
        Object.keys(users).forEach(function(name){
            client.emit('add gamer', name);
        });
        users[name] = [[0,0], [0, 0], [0,0]];


//        redisClient.sadd('gamers', name);
//        redisClient.smembers('gamers', function(err, names) {
//            client.broadcast.emit('reduce price', names.length);
//            client.emit('reduce price', names.length);
//
//            names.forEach(function(name){
//                client.emit('add gamer', name);
//            });
//        });
    });
});

server.listen(port);